class Flow < ApplicationRecord

  serialize :children_id, Array

  has_many :movements
  has_many :movement_field

  enum side: { camo: 1, purchasing: 2, transports: 3, repairs: 4, storage: 5, receiving: 6, shipping: 7, system: 8  }
  enum direction: { in: 0, out: 1 }

  def children
    Flow.where(id: self.children_id).all
  end

  def self.storage_starter_flows
    [550001,550002,550003]
  end

end