class Movement < ApplicationRecord
  
  belongs_to :step
  belongs_to :flow

  serialize :details, Hash

  after_create_commit :perform_system_step,  if: -> {step_is_open? && step_is_system?}
  after_create_commit :evaluate_opening_next_step, if: -> {last_step_is_closed? && step_is_closed?}

  after_create_commit :print_shit

  def print_shit
    #puts "Unit is #{self.unit_is_open?} and Step is #{self.step_is_open?}. System is System? #{self.step_is_system?}"
  end

  def evaluate_opening_next_step
    open_next_step(self.step.unit, Flow.find(self.step.closed_with.flow_id).children.first)
  end

  def perform_system_step
    puts ">> Is System Step"
    if self.flow_id == 800001
      if self.step.unit.serialised?
        @close_with = 850001 #Unit is serialised. Evaluating Open Orders
      else
        @close_with = 850002 #Unit is NOT serialised. Directing flow
      end 
    elsif self.flow_id == 800002
      puts ">> Is 800002"
      details = Movement.where("id < #{self.id}").where(flow_id: 450001).last.details
      if details['average_repair_cost_YTD_above_65%_of_catalog_price'].to_i == 1 || details['average_purchase_cost_YTD_above_85%_of_catalog_price'].to_i == 1
        puts ">> Repair is not Feasible"
        @close_with = 850006 #Repair NOT feasible. Directing Flow
      else
        puts ">> Repair is Feasible"
        @close_with = 850004
        # to be written
      end
    else
      return false
    end  
    puts ">> Writing..."
    close_step(self.step, Flow.find(@close_with))
  end

  def step_is_system?
    if self.flow.side == "system" then true else false end
  end

  def step_is_closed?
    self.step.closed?
  end

  def step_is_open?
    !self.step_is_closed?
  end

  def last_step_is_closed?
    self.step.unit.steps.last.closed?
  end

  def last_step_is_open?
    !last_step_is_closed?
  end

  protected

  def open_next_step(unit, next_step)
    step = unit.steps.build(expires_at: Time.current + next_step.sla.to_i * 60).movements.build(flow_id: next_step.id)
    if step.save
      update_unit_parameters(unit)
    end
  end

  def update_unit_parameters(unit)
    unit.open = true
    unit.responsible = unit.steps.last.responsible.downcase
    unit.expires_at = unit.steps.last.expires_at
    unit.save
  end

  def close_step(step, flow)
    movement = step.movements.build(flow_id: flow.id)
    movement.save
  end

end
