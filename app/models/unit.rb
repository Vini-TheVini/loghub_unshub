class Unit < ApplicationRecord

    validates :part_number, presence: true
    validates_presence_of :serial_number, :if => :serialised?
    validates_absence_of :serial_number, :unless => :serialised?

    has_many :steps, dependent: :destroy
    has_many :movements, through: :steps
    
    accepts_nested_attributes_for :steps
    validates_presence_of :steps

    enum responsible: Flow.sides

    def closed?
        !self.open?
    end

    def expires_at
        self.steps.last.expires_at
    end

end
