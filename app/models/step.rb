class Step < ApplicationRecord

  default_scope { order(created_at: :asc) }

  belongs_to :unit
  has_many :movements, dependent: :destroy

  accepts_nested_attributes_for :movements
  
  validates_presence_of :movements, :expires_at

  def closed?
    if self.movements.joins(:flow).where("flows.direction": "out").count == 1 then true else false end
  end

  def open?
    !self.closed?
  end

  def closed_with
    if self.closed?
      self.movements.joins(:flow).where("flows.direction": "out").first
    else
      return false
    end
  end

  def closed_at
    if self.closed?
      self.movements.joins(:flow).order("flows.direction").last.created_at
    else 
      return false
    end
  end

  def expired?
    if self.open?
      if self.expires_at < Time.current then true else false end
    else
      if self.expires_at < self.closed_at then true else false end
    end
  end

  def responsible
    if self.movements.first.flow.side == "camo"
      return "CAMO"
    else
      return self.movements.first.flow.side.humanize
    end
  end

  def icon
    case self.movements.first.flow.side
    when "camo"
      return "plane-departure"
    when "purchasing"
      return "shopping-cart"
    when "transports"
      return "truck-moving"
    when "repairs"
      return "tools"
    when "storage"
      return "warehouse"
    when "receiving" 
      return "boxes"
    when "shipping"
      return "people-carry"
    when "system"
      return "laptop"
    else
      return false
    end
  end

end
