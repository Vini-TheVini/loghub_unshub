class DashboardController < ApplicationController
  before_action :set_responsible

  def index
    @units = Unit.where(open: true, responsible: @responsible.downcase).all
  end

  private

  def set_responsible
    unless Flow.sides.select { |key, val| key.include?(params[:responsible].downcase)}.blank?
      if params[:responsible].downcase == "camo"
        @responsible = "CAMO"
      else
        @responsible = params[:responsible].humanize
      end
    else
      raise ActionController::RoutingError.new("A Dashboard for '#{params[:responsible].humanize}' coulndn't be found")
    end
  end
end
