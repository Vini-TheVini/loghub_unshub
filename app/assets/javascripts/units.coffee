# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'change', 'select', (e) ->
  showForm = $('div.step.details-fields#' + $(this).val())
  showForm.removeClass 'd-none'
  showForm.find('.form-control').attr 'disabled', false
  showForm.siblings('div.step.details-fields').addClass 'd-none'
  showForm.siblings('div.step.details-fields').find('.form-control').attr 'disabled', true
  return