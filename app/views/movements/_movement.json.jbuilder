json.extract! movement, :id, :step_uid, :flow_id, :created_at, :updated_at
json.url movement_url(movement, format: :json)
