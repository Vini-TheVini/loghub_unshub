json.extract! unit, :id, :part_number, :serialised, :serial_number, :created_at, :updated_at
json.url unit_url(unit, format: :json)
