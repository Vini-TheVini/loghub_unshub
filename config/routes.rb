Rails.application.routes.draw do

  get 'units/dashboard/:responsible', :to => 'dashboard#index'

  resources :units do
    resources :steps, only: [:index, :show] do
      resources :movements
    end
  end

  root 'units#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
