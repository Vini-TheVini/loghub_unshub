# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_22_181601) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "flows", force: :cascade do |t|
    t.integer "side", null: false
    t.integer "direction", null: false
    t.string "title", null: false
    t.string "children_id"
    t.integer "sla", comment: "SLA in minutes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "movement_fields", force: :cascade do |t|
    t.string "name"
    t.string "field_type"
    t.boolean "required"
    t.bigint "flow_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["flow_id"], name: "index_movement_fields_on_flow_id"
  end

  create_table "movements", force: :cascade do |t|
    t.bigint "flow_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "step_id"
    t.text "details"
    t.index ["flow_id"], name: "index_movements_on_flow_id"
    t.index ["step_id"], name: "index_movements_on_step_id"
  end

  create_table "steps", force: :cascade do |t|
    t.bigint "unit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "expires_at", null: false
    t.index ["unit_id"], name: "index_steps_on_unit_id"
  end

  create_table "units", force: :cascade do |t|
    t.string "part_number", null: false
    t.boolean "serialised"
    t.string "serial_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "open", default: true, null: false
    t.integer "responsible", default: 8, null: false
    t.datetime "expires_at"
  end

  add_foreign_key "movement_fields", "flows"
  add_foreign_key "movements", "flows"
  add_foreign_key "movements", "steps"
  add_foreign_key "steps", "units"
end
