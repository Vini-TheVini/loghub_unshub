class AddDetailsToMovements < ActiveRecord::Migration[5.2]
  def change
    add_column :movements, :details, :text
  end
end
