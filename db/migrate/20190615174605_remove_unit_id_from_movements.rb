class RemoveUnitIdFromMovements < ActiveRecord::Migration[5.2]
  def change
    remove_column :movements, :unit_id
    change_table :movements do |t|
      t.references :step, foreign_key: true
    end
  end
end
