class RemoveUidFromMovements < ActiveRecord::Migration[5.2]
  def change
    remove_column :movements, :step_uid
  end
end
