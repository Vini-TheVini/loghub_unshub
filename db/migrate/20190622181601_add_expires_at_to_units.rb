class AddExpiresAtToUnits < ActiveRecord::Migration[5.2]
  def change
    add_column :units, :expires_at, :datetime
  end
end
