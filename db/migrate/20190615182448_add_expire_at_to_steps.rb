class AddExpireAtToSteps < ActiveRecord::Migration[5.2]
  def change
    change_table :steps do |t|
      t.datetime :expires_at, null: false
    end
  end
end
