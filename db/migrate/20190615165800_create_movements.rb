class CreateMovements < ActiveRecord::Migration[5.2]
  def change
    create_table :movements do |t|
      t.references :flow, foreign_key: true
      t.references :unit, foreign_key: true
      t.string :step_uid, null:false
      
      t.timestamps
    end
  end
end
