class CreateFlows < ActiveRecord::Migration[5.2]
  def change
    create_table :flows do |t|
      t.integer :side, null: false
      t.integer :direction, null: false
      t.string :title, null: false
      t.string :children_id
      t.integer :sla, comment: "SLA in minutes"
      t.timestamps
    end
  end
end
