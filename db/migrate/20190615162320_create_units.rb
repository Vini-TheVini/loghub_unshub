class CreateUnits < ActiveRecord::Migration[5.2]
  def change
    create_table :units do |t|
      t.string :part_number, null: false
      t.boolean :serialised
      t.string :serial_number

      t.timestamps
    end
  end
end
