class AddColumnsToUnits < ActiveRecord::Migration[5.2]
  def change
    add_column :units, :open, :boolean, null:false, default: 1
    add_column :units, :responsible, :integer, null:false, default: 8
  end
end
